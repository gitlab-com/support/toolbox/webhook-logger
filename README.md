# Webhook Logger

Helper to make it easier to test check webhook payloads

## Usage

Designed to be run in a container with the port exposed. Logs to STDOUT, with an additional json only log file in the `log` directory

```
docker run -v $PWD:"/app/log" -p 7444:7444 registry.gitlab.com/gitlab-com/support/toolbox/webhook-logger
```

## Logs

- JSON Log output in `requests_json.log`
- Added `timestamp` field in `Time.now.iso8601`
