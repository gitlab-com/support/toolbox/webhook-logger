FROM ruby:3.0.3

# Initial Setup
RUN mkdir -p /app
WORKDIR /app
RUN echo 'gem: --no-ri --no-rdoc' > /root/.gemrc

# Copy Gem Files
COPY Gemfile* /app/
RUN cd /app && bundle install

# Copy Application Files
COPY app.rb /app/
COPY src /app/src/

EXPOSE 7444/tcp

CMD ["bundle", "exec", "app.rb"]