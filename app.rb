#!/usr/bin/env ruby

# Load Project Gems
require 'bundler/setup'
Bundler.require(:default)

require_all 'src'

post '/' do
  if payload.nil? || object_kind.nil?
    # Skip MIA Payloads
    status 204
  else
    write_log_event
    stdout_log_event
  end
end
