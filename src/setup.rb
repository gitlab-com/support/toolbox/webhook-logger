# Sinatra Settings
set :bind, '0.0.0.0'
set :port, 7444
set :server, :puma

# Oj Settings
Oj.default_options = { symbol_keys: true, mode: :compat }

# Amazing Print
AmazingPrint.defaults = {
  ruby19_syntax: true,
  index: false,
  indent: 2
}

# HashDot
Hash.use_dot_syntax = true
Hash.hash_dot_use_default = true

# Log Directory
Dir.mkdir 'log' unless Dir.exist? 'log'
