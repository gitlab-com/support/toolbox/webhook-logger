# Payload Helpers
module SinatraHelpers
  # Get Payload Data
  def payload
    @payload ||= parse_payload
  end

  def parse_payload
    request.body.rewind # Ensure Ready
    data = request.body.read
    @payload = data.empty? ? nil : Oj.load(data)
  end

  def object_kind
    payload.object_kind
  end

  def event_type
    payload.event_type
  end

  def event_name
    payload.event_name
  end

  def log_event
    logger.info "[#{object_kind}:#{event_type}] "
  end

  def time_now
    Time.now.strftime('%y-%m-%e--%H-%M')
  end

  # def event_file_name
  #   "#{dir}/#{time_now}_#{object_kind}_#{event_type || event_name}.json"
  # end

  def log_file
    'log/requests_json.log'
  end

  # def dir
  #   ENV.fetch('HOOK_DIR', nil)
  # end

  def payload_dump
    "#{Oj.dump(payload)}\n"
  end

  def write_log_event
    payload.timestamp = Time.now.iso8601
    # File.write(event_file_name, payload.ai(plain: true))
    File.open(log_file, 'a') { |f| f.write payload_dump }
  end

  def short_values
    payload.select { |_k, v| v.instance_of? String }
  end

  def stdout_log_event
    puts "============[ #{payload.object_kind} ]============"
    puts "Keys\n  #{payload.keys.ai(multiline: false)}\n"
    puts

    puts 'Top Values'
    puts payload.filter { |_k, v| v.instance_of? String }.ai
    puts

    puts 'Full'
    puts payload.ai
  end
end

helpers SinatraHelpers
